package com.alexgladkov.blackmail;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    View mDecorView;
    boolean isToggled = false;
    static String timeKey = "HoldTime";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0);
        String time = sharedPreferences.getString(timeKey, "");
        if (time.isEmpty()) {
            sharedPreferences.edit().putString(timeKey, "0.52").apply();
        }

        mDecorView = getWindow().getDecorView();

        ImageView btnStart = (ImageView) findViewById(R.id.btnStart);
        LinearLayout btnSettings = (LinearLayout) findViewById(R.id.llSettings);

        LinearLayout btnFacebook = (LinearLayout) findViewById(R.id.llFacebook);
        LinearLayout btnTwitter = (LinearLayout) findViewById(R.id.llTwitter);
        LinearLayout btnTelegram = (LinearLayout) findViewById(R.id.llTelegram);
        LinearLayout btnWhatsapp = (LinearLayout) findViewById(R.id.llWhatsapp);
        LinearLayout btnViber = (LinearLayout) findViewById(R.id.llViber);
        LinearLayout btnVk = (LinearLayout) findViewById(R.id.llVk);
        LinearLayout btnInstagram = (LinearLayout) findViewById(R.id.llInstagram);
        LinearLayout btnOk = (LinearLayout) findViewById(R.id.llOk);

        btnFacebook.setOnClickListener(clickListener);
        btnTwitter.setOnClickListener(clickListener);
        btnTelegram.setOnClickListener(clickListener);
        btnWhatsapp.setOnClickListener(clickListener);
        btnViber.setOnClickListener(clickListener);
        btnVk.setOnClickListener(clickListener);
        btnInstagram.setOnClickListener(clickListener);
        btnOk.setOnClickListener(clickListener);

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Settings.canDrawOverlays(MainActivity.this)) {
                    launchMainService();
                } else {
                    checkDrawOverlayPermission();
                }
            }
        });
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openSocialNetwork(Integer.valueOf((String) v.getTag()));
        }
    };

    private void openSocialNetwork(int position) {
        switch (position) {
            case 0:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/645852545797343/"));
                startActivity(browserIntent);
                break;

            case 1:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/blackmail_us_/"));
                startActivity(browserIntent);
                break;

            case 2:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/blackma44258713?s=09"));
                startActivity(browserIntent);
                break;

            case 3:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/blackmail_us"));
                startActivity(browserIntent);
                break;

            case 4:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/blackmail"));
                startActivity(browserIntent);
                break;

            case 5:
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "The text you wanted to share");
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                }
                break;

            case 6:
                Intent viberIntent = new Intent(Intent.ACTION_SEND);
                viberIntent.setType("text/plain");
                viberIntent.setPackage("com.viber.voip");
                viberIntent.putExtra(Intent.EXTRA_TEXT, "The text you wanted to share");
                try {
                    startActivity(viberIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Viber have not been installed.", Toast.LENGTH_LONG).show();
                }
                break;

            case 7:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ok.ru/group/54100000309456"));
                startActivity(browserIntent);
                break;

            default:
                break;
        }
    }

    private void hideSystemUI() {
        mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI() {
        mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void launchMainService() {

        Intent svc = new Intent(this, MainService.class);
        startService(svc);
        finish();
    }

    public final static int REQUEST_CODE = 10101;

    public void checkDrawOverlayPermission() {
        // Checks if app already has permission to draw overlays
        if (!Settings.canDrawOverlays(this)) {

            // If not, form up an Intent to launch the permission request
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));

            // Launch Intent, with the supplied request code
            startActivityForResult(intent, REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Check if a request code is received that matches that which we provided for the overlay draw request
        if (requestCode == REQUEST_CODE) {

            // Double-check that the user granted it, and didn't just dismiss the request
            if (Settings.canDrawOverlays(this)) {

                // Launch the service
                launchMainService();
            } else {

                Toast.makeText(this, "Sorry. Can't draw overlays without permission...", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
