package com.alexgladkov.blackmail;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

/**
 * Created by Alex Gladkov on 28.07.18.
 * Activity for settings
 * <div>Icons made by <a href="https://www.flaticon.com/authors/cole-bemis" title="Cole Bemis">
 *     Cole Bemis</a> from <a href="https://www.flaticon.com/" title="Flaticon"
 *     >www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
 *     title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
 */
public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0);
        final TextView txtTime = (TextView) findViewById(R.id.txtSettingsTime);
        ImageView btnBack = (ImageView) findViewById(R.id.imgBackButton);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtTime.setText(sharedPreferences.getString(MainActivity.timeKey, "0.52"));
        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), txtTime);
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.menu_time, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0);

                        switch (item.getItemId()) {
                            case R.id.settings_052:
                                sharedPreferences.edit().putString(MainActivity.timeKey, "0.52").apply();
                                txtTime.setText("0.52");
                                break;

                            case R.id.settings_044:
                                sharedPreferences.edit().putString(MainActivity.timeKey, "0.44").apply();
                                txtTime.setText("0.44");
                                break;

                            case R.id.settings_036:
                                sharedPreferences.edit().putString(MainActivity.timeKey, "0.36").apply();
                                txtTime.setText("0.36");
                                break;

                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }
}
