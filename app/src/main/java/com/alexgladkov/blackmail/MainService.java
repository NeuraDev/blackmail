package com.alexgladkov.blackmail;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Alex Gladkov on 29/07/2018.
 */

public class MainService extends Service implements View.OnTouchListener {
    private final Handler handler = new Handler();
    private static final String TAG = MainService.class.getSimpleName();
    private boolean isDrawed = false;
    private WindowManager windowManager;

    private View floatyView;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        addOverlayView();
//        startTick();
    }

//    private void startTick() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                isDrawed = !isDrawed;
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        drawScreen();
//                        startTick();
//                    }
//                });
//            }
//        }).start();
//    }

    private void addOverlayView() {
        final WindowManager.LayoutParams params =
                new WindowManager.LayoutParams(
                        isDrawed ? WindowManager.LayoutParams.MATCH_PARENT : WindowManager.LayoutParams.WRAP_CONTENT,
                        isDrawed ? WindowManager.LayoutParams.MATCH_PARENT : WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        0,
                        PixelFormat.TRANSLUCENT);

        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        params.flags = params.flags | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        params.flags = params.flags | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.flags = params.flags | WindowManager.LayoutParams.FLAG_FULLSCREEN;
        params.flags = params.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
        params.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;

        FrameLayout interceptorLayout = new FrameLayout(this) {
            boolean downIsReady = false;
            boolean upIsReady = false;
            long tresholdTime = 2;
            long volumeDownStartTime = 0;
            long volumeDownEndTime = 0;
            long volumeUpStartTime = 0;
            long volumeUpEndTime = 0;

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (isDrawed && event.getKeyCode() == KeyEvent.KEYCODE_BACK) return true;

                volumeDownEndTime = System.currentTimeMillis();
                volumeUpEndTime = System.currentTimeMillis();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN && volumeDownStartTime == 0) {
                        volumeDownStartTime = System.currentTimeMillis();
                    } else if (volumeDownStartTime > 0) {
                        long diff = (volumeDownEndTime - volumeDownStartTime) / 1000;
                        if (diff > tresholdTime) {
                            downIsReady = true;
                            if (upIsReady) {
                                drawScreen();
                            }
                            volumeDownStartTime = -1;
                        }

                    }

                    if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP && volumeUpStartTime == 0) {
                        volumeUpStartTime = System.currentTimeMillis();
                    } else if (volumeUpStartTime > 0) {
                        long diff = (volumeUpEndTime - volumeUpStartTime) / 1000;
                        if (diff > tresholdTime) {
                            upIsReady = true;
                            if (downIsReady) {
                                drawScreen();
                            }
                            volumeUpStartTime = -1;
                        }
                    }

                    if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN ||
                            event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP)
                        return volumeDownStartTime > 0 && volumeUpStartTime > 0;
                }

                if (event.getAction() == KeyEvent.ACTION_UP) {
                    downIsReady = false;
                    upIsReady = false;
                    volumeDownStartTime = 0;
                    volumeDownEndTime = 0;
                    volumeUpStartTime = 0;
                    volumeUpEndTime = 0;

                    return super.dispatchKeyEvent(event);
                }

                // Otherwise don't intercept the event
                return isDrawed || super.dispatchKeyEvent(event);
            }
        };

        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        if (inflater != null) {
            floatyView = inflater.inflate(R.layout.floating_view, interceptorLayout);
            floatyView.setOnTouchListener(this);
            windowManager.addView(floatyView, params);
        }
    }

    public void drawScreen() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0);
        String time = sharedPreferences.getString(MainActivity.timeKey, "0.52");
        final int intTime = (int) (Float.valueOf(time) * 1000);

        if (floatyView != null) {
            int height = Resources.getSystem().getDisplayMetrics().heightPixels;
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            final TextView textView = (TextView) floatyView.findViewById(R.id.txtBlackMail);
            final Handler handler = new Handler();

            isDrawed = !isDrawed;
            if (isDrawed) {
                floatyView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

                final WindowManager.LayoutParams wmParams =
                        new WindowManager.LayoutParams(
                                WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.TYPE_PHONE,
                                0,
                                PixelFormat.TRANSLUCENT);

                wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
                wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
                wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_FULLSCREEN;
                wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                wmParams.gravity = Gravity.CENTER;
                wmParams.x = 0;
                wmParams.y = 0;

                windowManager.removeView(floatyView);
                windowManager.addView(floatyView, wmParams);

                floatyView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                textView.setVisibility(View.VISIBLE);
                textView.setText("We Are The Resistance");

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) floatyView.findViewById(R.id.flBlackMail).getLayoutParams();
                params.width = width;
                params.height = height;
                floatyView.findViewById(R.id.flBlackMail).setLayoutParams(params);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText("picture 2");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                textView.setText(getString(R.string.app_name).toUpperCase());
                            }
                        }, intTime / 2);
                    }
                }, intTime / 2);
            } else {
                textView.setText("They Don't Care About US");

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText("picture 4");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                floatyView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

                                final WindowManager.LayoutParams wmParams =
                                        new WindowManager.LayoutParams(
                                                WindowManager.LayoutParams.WRAP_CONTENT,
                                                WindowManager.LayoutParams.WRAP_CONTENT,
                                                WindowManager.LayoutParams.TYPE_PHONE,
                                                0,
                                                PixelFormat.TRANSLUCENT);

                                wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
                                wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
                                wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_FULLSCREEN;
                                wmParams.gravity = Gravity.CENTER;
                                wmParams.x = 0;
                                wmParams.y = 0;

                                windowManager.removeView(floatyView);
                                windowManager.addView(floatyView, wmParams);

                                floatyView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
                                textView.setVisibility(View.GONE);

                                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) floatyView.findViewById(R.id.flBlackMail).getLayoutParams();
                                params.width = 0;
                                params.height = 0;
                                floatyView.findViewById(R.id.flBlackMail).setLayoutParams(params);
                            }
                        }, intTime / 2);
                    }
                }, intTime / 2);
            }
        }
    }

    @Override
    public void onDestroy() {
        if (isDrawed) {
            Intent svc = new Intent(this, MainService.class);
            startService(svc);
        }
        super.onDestroy();

//        if (floatyView != null) {
//            windowManager.removeView(floatyView);
//            floatyView = null;
//        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        Log.v(TAG, "onTouch...");

        // Kill service
//        onDestroy();

        return true;
    }
}
